clear all;
close all;
clc;
% inputpath = 'D:\phd\dataset\ICDAR2013Dataset\images_train\images_train\';
inputpath = 'D:\phd\dataset\ICDAR2013Dataset\images_test\images_test\';
% inputpath = 'D:\phd\dataset\kannada\BWOriginalTxtNoiseRemove\BWOriginalTxtNoiseRemove\';
% inputpath = 'D:\phd\dataset\OriyaPart\BWOriginalTxt\BWOriginalTxt\';
% inputpath = 'D:\phd\dataset\PersianPart\BWOriginalTxt\BWOriginalTxt\';
% inputpath = 'D:\phd\dataset\kannada\BWOriginalTxtNoiseRemove\BWOriginalTxtNoiseRemove\';
% % inputpath = 'D:\phd\dataset\PersianPart\BWOriginalTxt\BWOriginalTxt\';
% inputpath = 'D:\phd\dataset\MARGdataset\';
% outputpath = 'D:\phd\coding\tunneling\MARGdataset\';
outputpath = 'D:\phd\coding\separator\result1\';
for f=301:304
    close all;
    inputfilename = strcat(inputpath, '', int2str(f), '.tif');
    original = imread(inputfilename);
    original = original(:,:,1);
    bw = im2bw(original, 0.5);
    bw = ~bw;
    run = comp(bw);
    whiterun = run(:, 1);
    whiteruntrans = whiterun';
    offset = 2500/20;
    det = detect( whiterun, offset );
    fband = findband( det );
    if fband(1, 1) ~= 0
        for k = 1:size(fband, 1)
            partwhiterun = whiterun(fband(k, 1):fband(k, 2));
            partdet = detect( partwhiterun, offset );
            det(fband(k, 1):fband(k, 2)) = partdet;
            det(fband(k, 1):fband(k, 1)+20) = 0;
            det(fband(k, 2)-20:fband(k, 2)) = 0;
        end
    end
    
    
    fband = findband( det );
    if fband(1, 1) ~= 0
        for k = 1:size(fband, 1)
            partwhiterun = whiterun(fband(k, 1):fband(k, 2));
            partdet = detect( partwhiterun, offset );
            det(fband(k, 1):fband(k, 2)) = partdet;
            det(fband(k, 1):fband(k, 1)+30) = 0;
            det(fband(k, 2)-30:fband(k, 2)) = 0;
        end
    end
    
    
    det(1957, 1) = 1.01;
    plot(det, 1:size(det,1));
    set(gca,'Ydir','Reverse');
    set(gca, 'XAxisLocation', 'top');
    img = drawsample(bw, det, 1);
    figure, imshow(~img);
    smallgap = findsmallgap( det );
    if smallgap(1, 1) ~= 0
        for k = 1:size(smallgap, 1)
            det(smallgap(k, 1):smallgap(k, 2)) = 0;
        end
    end
%     det(1957, 1) = 1.01;
%     plot(det, 1:size(det,1));
%     set(gca,'Ydir','Reverse');
%     set(gca, 'XAxisLocation', 'top');
%     img = drawsample(bw, det, 1);
%     figure, imshow(~img);
    lmid = midpoint( det );
%     lmid(1957, 1) = 1.01;
%     plot(lmid, 1:size(lmid,1));
%     set(gca,'Ydir','Reverse');
%     set(gca, 'XAxisLocation', 'top');
    img = drawsample(bw, lmid, 1);    
%     figure, imshow(~img);
%     outputfilename = strcat(outputpath, '', int2str(f), 'out.tif');
%     imwrite(~img, outputfilename); 

    img = ~img;
    offset = 2500/10;
    fwhiterun = finalwhite(run);
    det = detect( fwhiterun, offset );
    fband = findband( det );
    if fband(1, 1) ~= 0
        for k = 1:size(fband, 1)
            partwhiterun = fwhiterun(fband(k, 1):fband(k, 2));
            partdet = detect( partwhiterun, offset );
            det(fband(k, 1):fband(k, 2)) = partdet;
    %         det(fband(k, 1):fband(k, 1)+20) = 0;
    %         det(fband(k, 2)-20:fband(k, 2)) = 0;

        end
    end
    smallgap = findsmallgap( det );
    if smallgap(1, 1) ~= 0
        for k = 1:size(smallgap, 1)
            det(smallgap(k, 1):smallgap(k, 2)) = 0;
        end
    end
%     img = drawsample(img, det, 2);
    img = ~img;
    lmid = midpoint( det );
%     lmid(1957, 1) = 1.01;
    img = drawsample(img, lmid, 2); 
    outputfilename = strcat(outputpath, '', int2str(f), '.tif');
    imwrite(~img, outputfilename); 
%     imshow(img);
end
