function [ out ] = drawsample(bw, vector, direction)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    [h, w] = size(vector);

    for i= 1:h
        if vector(i, 1) >= 1
            if direction == 1
                for j = 1:vector(i,1)*100
                    bw(i, j) = 1;
                end
            else
                for j = size(bw,2)-vector(i,1)*100:size(bw,2)
                    bw(i, j) = 1;
                end
            end
        end
    end
    out = bw;
end

