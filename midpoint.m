function [ out ] = midpoint( inp )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [h, w] = size(inp);
    
    s = 0;
    out(1, 1) = 0;
    if inp(1,1) == 1
        s = 1;
    end
    for i=2:h
        out(i, 1) = 0;
        if inp(i-1, 1) == 0 && inp(i, 1) == 1 && s == 0
            s = i;
        end
        if inp(i-1, 1) == 1 && inp(i, 1) == 0 && s > 0
            out(ceil((s+i)/2), 1) = 1;
            s = 0;
        end
    end
    if s ~= 0
        out(floor((s+h)/2), 1) = 1;
    end
end