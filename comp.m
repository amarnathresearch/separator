function [ output ] = comp( input )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [height width] = size(input);
    for i=1:height
        c = 1;
        p = 1;
        if input(i,1) ~= 0
            output(i, 1) = 0;
            p = p + 1;
        end
        for j=2:width
            if input(i,j-1)== input(i,j)
                c = c + 1;
            else
                output(i, p) = c;
                p = p + 1;
                c = 1;
            end
        end
        output(i, p) = c;
    end

end

