function [ output ] = decomp( input )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [height width] = size(input);
    for i=1:height
        inten = 0;
        p = 1;
        for j=1:width

            c = input(i,j);
            for k=1:c
                output(i,p) = inten;
                p = p + 1;
            end
            if inten == 1
                inten = 0;
            else
                inten = 1;
            end
        end
    end

end

