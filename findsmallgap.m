function [ out ] = findsmallgap( inp )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [r, c] = size( inp );
    s = 0;
    threshold = 20;
    cnt = 1;
    out(cnt, 1) = 0;
    out(cnt, 2) = 0;
    if inp(1,1) == 0
        s = 1;
    end
    for i=2:r
        if inp(i-1, 1) > 0 && inp(i, 1) == 0;
            s = i;
        end
        if inp(i-1, 1) == 0 && inp(i, 1) > 0 && s > 0
            if (i - s) <= threshold
                sct = 0;
                for b = s:-1:1
                    sct = sct + 1;
                    if b-1 == 1
                        break;
                    elseif inp(b-1, 1) == 0
                        break;
                    end
                end
                ect = 0;
                for b = i:r
                    ect = ect + 1;
                    if b+1 == r
                        break;
                    elseif inp(b+1, 1) == 0
                        break;
                    end
                end
                if sct < ect
                    out(cnt, 1) = s-sct;
                    if out(cnt, 1) < 1
                        out(cnt, 1) = 1;
                    end
                    out(cnt, 2) = s;
                else
                    out(cnt, 1) = i-1;
                    out(cnt, 2) = i+ect;
                    if out(cnt, 2) > r
                        out(cnt, 2) = r;
                    end
                end
                cnt = cnt + 1;
            end
            s = 0;
        end
    end
    if s ~= 0
        if (r - s) <= threshold
            out(cnt, 1) = s;
            out(cnt, 2) = r;
        end
    end
end

