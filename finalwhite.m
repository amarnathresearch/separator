function [ out ] = finalwhite(run)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    [h, w] = size(run);
    out(1, 1) = 0;
    for i = 1:h
        out(i,1) = 0;
        j = w;
        while j > 0
            if mod(j, 2) ~= 0
                if run(i, j) > 0
                    out(i, 1) = run(i, j);
                    break;
                end
            end
            j = j - 1;
        end
    end
end
