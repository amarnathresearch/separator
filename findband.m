function [ out ] = findband( inp )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [r, c] = size( inp );
    threshold = r/30;
    s = 0;
    cnt = 1;
    out(cnt, 1) = 0;
    out(cnt, 2) = 0;
    if inp(1,1) > 0
        s = 1;
    end
    for i=2:r
        if inp(i-1, 1) == 0 && inp(i, 1) > 0;
            s = i;
        end
        if inp(i-1, 1) > 0 && inp(i, 1) == 0 && s > 0
            if (i - s) > threshold
                out(cnt, 1) = s;
                out(cnt, 2) = i-1;
                cnt = cnt + 1;
            end
            s = 0;
        end
    end
    if s ~= 0
        if (r - s) > threshold
            out(cnt, 1) = s;
            out(cnt, 2) = r;
        end
    end
end

