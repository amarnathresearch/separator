function [ out ] = detect( whiterun, offset)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    minval = min(whiterun);
    [r, c] = size(whiterun);
%     offset = 2500/20;
    out(1, 1) = 0;
    for i = 1:r
        whiterun(i, 1) = whiterun(i, 1) - minval;
        if whiterun(i, 1) >= (2*offset)
            out(i, 1) = 1;
        elseif whiterun(i, 1) > offset
            out(i, 1) = 1;
        else
            out(i, 1) = 0;
        end
    end
end

